<?php

/**
 * @file
 * drush integration for filesize module.
 */

/**
 * URL of production version.
 */
define('FILESIZE_PROD_URL', 'http://cdn.filesizejs.com/filesize.min.js');

/**
 * URL of debug version.
 */
define('FILESIZE_DEBUG_URL', 'http://cdn.filesizejs.com/filesize.js');

/**
 * Implements hook_drush_command().
 */
function filesize_drush_command() {
  $items = array();

  $items['filesize-download'] = array(
    'callback' => 'filesize_drush_download',
    'description' => dt("Downloads the Filesize.js library."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'type' => '"debug" or "prod" (default is "prod")',
    ),
    'options' => array(
      'path' => dt('Optional. The path to your shared libraries. If omitted Drush will use the default location.'),
    ),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function filesize_drush_help($section) {
  switch ($section) {
    case 'drush:filesize-download':
      $path = 'sites/all/libraries';
      $msg = dt("Downloads Filesize.js. Default location is @path.",
               array('@path' => $path));
      return $msg;
  }
}

/**
 * Downloads Filesize.js.
 */
function filesize_drush_download($type = 'prod') {
  if (!drush_shell_exec('type unzip')) {
    return drush_set_error(dt('Missing dependency: unzip. Install it before using this command.'));
  }

  if (!in_array(strtolower($type), array('prod', 'debug'))) {
    return drush_set_error(dt('Wrong argument: Use "prod" or "debug".'));
  }

  if ($type == 'prod') {
    $download_url = FILESIZE_PROD_URL;
    $file = FILESIZE_PROD_FILE_NAME;
  } else {
    $download_url = FILESIZE_DEBUG_URL;
    $file = FILESIZE_DEBUG_FILE_NAME;
  }

  if (!$path = drush_get_option('path')) {
   $path =  'sites/all/libraries';
  }

  // Create path directory if needed.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory #@path was created', array('@path' => $path)), 'notice');
  }

  // Add the directory for storing filesize.
  $path .= "/" . FILESIZE_FOLDER_NAME;

  // Create the filesize path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory #@path was created', array('@path' => $path)), 'notice');
  }
  else {
    drush_log(dt('Directory @path exists and will be used', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Remove existing js if present.
  if (is_file($file)) {
    drush_op('unlink', $file);
  }

  // Download the JS.
  if (!drush_shell_exec('wget ' . $download_url)) {
    drush_shell_exec('curl -O ' . $download_url);
  }

  // Set working directory  back to the previous working directory.
  chdir($olddir);

  if (is_dir($path)) {
    drush_log(dt('@file has been downloaded to @path', array('@file' => $file, '@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download Filesize.js to @path', array('@path' => $path)), 'error');
  }
}
